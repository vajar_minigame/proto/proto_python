# Generated by the protocol buffer compiler.  DO NOT EDIT!
# sources: interactions.proto
# plugin: python-betterproto
from dataclasses import dataclass
from typing import Optional

import betterproto
import grpclib


@dataclass(eq=False, repr=False)
class MonConsumeItemRequest(betterproto.Message):
    item_id: "_common__.ItemId" = betterproto.message_field(1)
    mon_id: "_common__.MonId" = betterproto.message_field(2)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class UserConsumeItemRequest(betterproto.Message):
    item_id: "_common__.ItemId" = betterproto.message_field(1)
    user_id: "_common__.UserId" = betterproto.message_field(2)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class InteractionServiceCall(betterproto.Message):
    meta_data: "_common__.MetaData" = betterproto.message_field(1)
    consume_item: "MonConsumeItemRequest" = betterproto.message_field(2, group="method")

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class FeedMonResponse(betterproto.Message):
    pass

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class InteractionServiceResponse(betterproto.Message):
    meta_data: "_common__.MetaData" = betterproto.message_field(1)
    status: "_common__.ResponseStatus" = betterproto.message_field(2, group="data")

    def __post_init__(self) -> None:
        super().__post_init__()


class InteractionServiceStub(betterproto.ServiceStub):
    async def monster_consume_item(
        self, *, item_id: "_common__.ItemId" = None, mon_id: "_common__.MonId" = None
    ) -> "_common__.ResponseStatus":

        request = MonConsumeItemRequest()
        if item_id is not None:
            request.item_id = item_id
        if mon_id is not None:
            request.mon_id = mon_id

        return await self._unary_unary(
            "/interaction.InteractionService/MonsterConsumeItem",
            request,
            _common__.ResponseStatus,
        )

    async def user_consume_item(
        self, *, item_id: "_common__.ItemId" = None, user_id: "_common__.UserId" = None
    ) -> "_common__.ResponseStatus":

        request = UserConsumeItemRequest()
        if item_id is not None:
            request.item_id = item_id
        if user_id is not None:
            request.user_id = user_id

        return await self._unary_unary(
            "/interaction.InteractionService/UserConsumeItem",
            request,
            _common__.ResponseStatus,
        )


from .. import common as _common__
