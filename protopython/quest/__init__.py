# Generated by the protocol buffer compiler.  DO NOT EDIT!
# sources: quest.proto
# plugin: python-betterproto
from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional

import betterproto
import grpclib


@dataclass(eq=False, repr=False)
class Quest(betterproto.Message):
    id: "_common__.QuestId" = betterproto.message_field(1)
    difficulty: int = betterproto.int32_field(2)
    recommended_strength: int = betterproto.int32_field(3)
    player_id: int = betterproto.int32_field(4)
    # duration in seconds
    duration: int = betterproto.int32_field(5)
    active_quest: "ActiveQuest" = betterproto.message_field(6)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class ActiveQuest(betterproto.Message):
    start_time: datetime = betterproto.message_field(1)
    monster_ids: List["_common__.MonId"] = betterproto.message_field(2)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class StartQuestRequest(betterproto.Message):
    quest_id: "_common__.QuestId" = betterproto.message_field(1)
    monster_ids: List["_common__.MonId"] = betterproto.message_field(2)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class GetQuestsByUser(betterproto.Message):
    user_id: "_common__.UserId" = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class GetQuest(betterproto.Message):
    quest_id: "_common__.QuestId" = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class StartQuestResponse(betterproto.Message):
    status: "_common__.ResponseStatus" = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class GetQuestResponse(betterproto.Message):
    quest: "Quest" = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class GetQuestByUserResponse(betterproto.Message):
    quests: List["Quest"] = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class QuestList(betterproto.Message):
    quests: List["Quest"] = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class QuestCompletedEvent(betterproto.Message):
    quest_id: "_common__.QuestId" = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class QuestUpdatedEvent(betterproto.Message):
    quest: "Quest" = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class QuestDeletedEvent(betterproto.Message):
    quest_id: "_common__.QuestId" = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class QuestStartedEvent(betterproto.Message):
    quest: "Quest" = betterproto.message_field(1)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class QuestEvent(betterproto.Message):
    completed_event: "QuestCompletedEvent" = betterproto.message_field(1, group="event")
    updated_event: "QuestUpdatedEvent" = betterproto.message_field(2, group="event")
    deleted_event: "QuestDeletedEvent" = betterproto.message_field(3, group="event")
    started_event: "QuestStartedEvent" = betterproto.message_field(4, group="event")

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class QuestServiceCall(betterproto.Message):
    start_quest: "StartQuestRequest" = betterproto.message_field(1, group="method")
    get_quest: "GetQuest" = betterproto.message_field(2, group="method")
    quest_quest_by_user: "GetQuestsByUser" = betterproto.message_field(
        3, group="method"
    )
    meta_data: "_common__.MetaData" = betterproto.message_field(4)

    def __post_init__(self) -> None:
        super().__post_init__()


@dataclass(eq=False, repr=False)
class QuestServiceResponse(betterproto.Message):
    start_quest_response: "StartQuestResponse" = betterproto.message_field(
        1, group="data"
    )
    get_quest_response: "GetQuestResponse" = betterproto.message_field(2, group="data")
    get_quest_by_user_response: "GetQuestByUserResponse" = betterproto.message_field(
        3, group="data"
    )
    meta_data: "_common__.MetaData" = betterproto.message_field(4)

    def __post_init__(self) -> None:
        super().__post_init__()


class QuestServicesStub(betterproto.ServiceStub):
    async def add_quest(
        self,
        *,
        id: "_common__.QuestId" = None,
        difficulty: int = 0,
        recommended_strength: int = 0,
        player_id: int = 0,
        duration: int = 0,
        active_quest: "ActiveQuest" = None,
    ) -> "_common__.QuestId":

        request = Quest()
        if id is not None:
            request.id = id
        request.difficulty = difficulty
        request.recommended_strength = recommended_strength
        request.player_id = player_id
        request.duration = duration
        if active_quest is not None:
            request.active_quest = active_quest

        return await self._unary_unary(
            "/quest.QuestServices/AddQuest", request, _common__.QuestId
        )

    async def get_quest_by_id(self, *, id: int = 0) -> "Quest":

        request = _common__.QuestId()
        request.id = id

        return await self._unary_unary(
            "/quest.QuestServices/GetQuestByID", request, Quest
        )

    async def get_quest_by_user_id(self, *, id: int = 0) -> "QuestList":

        request = _common__.UserId()
        request.id = id

        return await self._unary_unary(
            "/quest.QuestServices/GetQuestByUserID", request, QuestList
        )

    async def start_quest(
        self,
        *,
        quest_id: "_common__.QuestId" = None,
        monster_ids: Optional[List["_common__.MonId"]] = None,
    ) -> "_common__.ResponseStatus":
        monster_ids = monster_ids or []

        request = StartQuestRequest()
        if quest_id is not None:
            request.quest_id = quest_id
        if monster_ids is not None:
            request.monster_ids = monster_ids

        return await self._unary_unary(
            "/quest.QuestServices/StartQuest", request, _common__.ResponseStatus
        )


from .. import common as _common__
